#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 15:56:22 2021

@author: archambault


"""

import numpy as np 
import os
import matplotlib.pyplot as plt
from path_manager import *
 
def make_square_data(path_load   =PATH_DATA):
    l=os.listdir(path_load) 
    L=[]
    for i in l:
        if ".npy" in i:
            L.append(i)
    print(L)
    
    
    for i in L:
        
        mat=np.load(path_load+"/"+i)
        ishape=mat.shape
        dim=min(ishape[1],ishape[2])
        mat=mat[:,0:dim,0:dim]
        
        print("initial size = ",ishape," to size : ",mat.shape)
        np.save(path_load+"/square_"+i[:len(i)-4],mat)
        


def merge_month(path_load   = "/home/pequan2/archambault/local/Data/SARGAS2008byVarSat/2008"):
    L=sorted(os.listdir(path_load) )
    
    SST=[l for l in L if "sst" in l]
    SSH=[l for l in L if "ssh" in l]
    U=[l for l in L if "_v_" in l]
    V=[l for l in L if "_u_" in l]
    
    print(SST)
    print(SSH)
    print(U)
    print(V)
    
    resolutions=["R01","R03","R09","R27","R81"]
    for r in resolutions:
        sst=[l for l in SST if r in l]
        ssh=[l for l in SSH if r in l]
        u=[l for l in U if r in l]
        v=[l for l in V if r in l]
        
        
        L=[sst,ssh,u,v]
        NAME=["SST","SSH","U","V"]
        c=0
        for tab in L:
            for k in range (len(tab)):
                if k==0:
                    x=np.load(path_load+ "/"+tab[k])
                else:
                    mat=np.load(path_load+ "/"+tab[k])
                    x= np.concatenate((x,mat),axis=0)
            
                print(x.shape)
            
            name="/Merged/NATL60_2008_"+NAME[c]+"_"+r
            np.save(path_load+name,x)
            c=c+1

def merge2013_2008(path_load1   = "/home/pequan2/archambault/local/Data/NATL60byVar",path_load2="/home/pequan2/archambault/local/Data/SARGAS2008byVarSat/2008/Merged"):
    
    L1=sorted(os.listdir(path_load1) )
    L1=sorted([l for l in L1 if "square" in l])
    L2=sorted(os.listdir(path_load2) )
    L2=sorted([l for l in L2 if "square" in l])
    print(L1)
    print(L2)
    
    print(len(L1),len(L2))
    
    for k in range(len(L1)):
        mat1=np.load(path_load1+"/"+L1[k])
        mat2=np.load(path_load2+"/"+L2[k])

        mat=np.concatenate((mat1,mat2),axis=0)
        np.save(path_load1+"/1308_"+L1[k],mat)
        
        print("Saved file in :" ,path_load1+"/1308_"+L1[k],"\n size =",mat.shape)


def downsamples(X,r):
    if X.shape[1]%r!=0 or X.shape[2]%r!=0:
        raise IndexError("Size not divisible by ratio")
    
    Xd=np.zeros((X.shape[0],X.shape[1]//r,X.shape[2]//r))
    for k in range (Xd.shape[0]):
        Xd[k,:]=X[k,:].reshape(-1, r, X.shape[1]//r, r).sum((-1,-3))/(r*r)        
    
    return Xd



def make_data():
    
    # You must put the 2 files :
    #     natl60_htuv_03_06_09_12-2008.npz
    #     natl60_htuv_01102012_01102013-2008.npz
    # In the folder that points to the PATH_DATA in path manager.
    
    test=np.load(PATH_DATA+"/natl60_htuv_03_06_09_12-2008.npz")
    train=np.load(PATH_DATA+"/natl60_htuv_01102012_01102013.npz")
    
    test_ssh_r01=test["FdataAllVar"][0,:,:,0:1296]
    test_sst_r01=test["FdataAllVar"][1,:,:,0:1296]
    train_ssh_r01=train["FdataAllVar"][0,:,:,0:1296]
    train_sst_r01=train["FdataAllVar"][1,:,:,0:1296]
    
    ssh_r01=np.concatenate((train_ssh_r01,test_ssh_r01),axis=0)
    sst_r01=np.concatenate((train_sst_r01,test_sst_r01),axis=0)
    
    ssh_r03=downsamples(ssh_r01,3)
    sst_r03=downsamples(sst_r01,3)
    np.save(PATH_DATA+"/1308_square_NATL60_SSH_R03",ssh_r03)
    np.save(PATH_DATA+"/1308_square_NATL60_SST_R03",sst_r03)
    print("R03 : done")
    
    ssh_r09=downsamples(ssh_r03,3)
    sst_r09=downsamples(sst_r03,3)
    np.save(PATH_DATA+"/1308_square_NATL60_SSH_R09",ssh_r09)
    np.save(PATH_DATA+"/1308_square_NATL60_SST_R09",sst_r09)
    print("R09 : done")

    
    ssh_r27=downsamples(ssh_r09,3)
    sst_r27=downsamples(sst_r09,3)
    np.save(PATH_DATA+"/1308_square_NATL60_SSH_R27",ssh_r27)
    np.save(PATH_DATA+"/1308_square_NATL60_SST_R27",sst_r27)
    print("R27 : done")

    
    ssh_r81=downsamples(ssh_r27,3)
    sst_r81=downsamples(sst_r27,3)
    np.save(PATH_DATA+"/1308_square_NATL60_SSH_R81",ssh_r81)
    np.save(PATH_DATA+"/1308_square_NATL60_SST_R81",sst_r81)
    print("R81 : done")


def make_reduce_data():
    SST=np.load("Data/SST_R03.npy")
    SSH=np.load("Data/SSH_R03.npy")
    
    np.save("Data/SSH_R03",SSH)
    np.save("Data/SST_R03",SST)
    
    SST=downsamples(SST,3)
    SSH=downsamples(SSH,3)
    np.save("Data/SSH_R09",np.expand_dims(SSH,3))
    np.save("Data/SST_R09",np.expand_dims(SST,3))
    
    SST=downsamples(SST,3)
    SSH=downsamples(SSH,3)
    np.save("Data/SSH_R27",np.expand_dims(SSH,3))
    np.save("Data/SST_R27",np.expand_dims(SST,3))
    
    SST=downsamples(SST,3)
    SSH=downsamples(SSH,3)
    np.save("Data/SSH_R81",np.expand_dims(SSH,3))
    np.save("Data/SST_R81",np.expand_dims(SST,3))
    
    
    





















