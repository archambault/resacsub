#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 15:57:25 2021

@author: archambault
"""

import math
from   matplotlib  import cm
import sys
from   time  import time
import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import random 
from sklearn.preprocessing import StandardScaler
import pickle

from tensorflow.keras.layers import Input, Dense, Flatten, Reshape, AveragePooling2D, Dropout, add 
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from tensorflow.keras.layers import Concatenate, concatenate, BatchNormalization,Lambda ,Activation
from tensorflow.keras.models    import Model
from tensorflow.keras.models    import load_model, model_from_json
import tensorflow.keras.callbacks
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras import regularizers
from tensorflow.keras import optimizers
from functools import partial

from tensorflow.nn import swish
from tensorflow.nn import depth_to_space
from tensorflow.nn import space_to_depth
from tensorflow.keras.callbacks import LearningRateScheduler

plt.close("all")

from tensorflow.keras import backend as K

from metrics import *
from path_manager import *


def MinMaxScaler(X):
    MIN=np.min(X)
    MAX=np.max(X)
    Xsc=(X-MIN)/(MAX-MIN)
    
    param={"min":MIN,"max":MAX}
    return Xsc, param

def inv_MinMaxScaler(Xsc,param):
    MAX=param["max"]
    MIN=param["min"]
    
    return (MAX-MIN)*Xsc+MIN


def denoising_model():
    init       = "glorot_uniform"
    #"he_normal"#"constant"#"he_normal" "zeros"glorot_uniform  random_normal
    
    
    IN=Input(shape=(432,432,1))
    arch=Conv2D(32, (7,7),padding="same",activation="relu")(IN)    
    arch=Conv2D(32, (7,7),padding="same",activation="relu")(arch)

    arch=Conv2D(1, (1,1),padding="same")(arch)
    

    return Model(IN,arch)
#class to store the params
class TrainParam:
    def __init__(self,lr,epochs,batch_size,verbose=1,test_num=0, size_test=73,size_plage=15,use_scheduler=False):
        
        self.lr=lr
        self.epochs=epochs
        self.batch_size=batch_size
        self.verbose=verbose
        self.test_num=test_num
        self.size_test=size_test
        self.size_plage=size_plage
        self.ind_train="not defined"
        self.ind_test="not defined"
        self.use_scheduler=use_scheduler
class NeuralStruct:
    def __init__(self,Nconv,Nfilt,function,factiv=swish,model_type=False,Incertitude =False,end_layers=True,pen=1e-3):
        
        self.Nconv=Nconv
        self.Nfilt=Nfilt
        self.function=function
        self.factiv=factiv
        self.model_type=model_type
        self.Incertitude=Incertitude
        self.end_layers=end_layers
        self.pen=pen
class DataTab:
    def __init__(self,kind="not defined",res="not defined",normalization="not defined",path="not defined",loss="not defined"):
        
        self.kind=kind
        self.res=res
        self.normalization=normalization
        self.path=path
        self.loss=loss
class Params:
    def __init__(self,name,model_type="not defined",):
        self.name=name
        self.model_type=model_type
        self.IN=[]
        self.OUT=[]
        self.nn="not defined"
        self.train_param="not defined"
        self.path_save="not defined"
        self.scaler_in=[]
        self.scaler_out=[]
        self.path_data="not defined"
#class to store the model structure
class modelStruct:
    def __init__(self,name,model_type="not defined",):
        self.params=Params(name,model_type)
        self.model="not defined"
        self.H="not defined"
        
    def addIN(self,kind="not defined",res="not defined",normalisation="not defined",path="not defined"):
        self.params.IN.append(DataTab(kind,res,normalisation,path))
        
    def addOUT(self,kind="not defined",res="not defined",normalisation="not defined",path="not defined",loss="not defined"):
        self.params.OUT.append(DataTab(kind,res,normalisation,path,loss))
    
    def printIN(self):
        print()
        if len(self.params.IN)==0:
            print("No input for %s model" %self.params.name)
        else:
            print(15*'*')
            print("INPUT for model %s :" %self.params.name)
            c=1
            for i in self.params.IN:
                print("IN",c," :\ttype =",i.kind,"\tres =",i.res,"\tnormalization =",i.normalization)
                c=c+1
            print(15*'*')
        print()
    def printOUT(self):
        print()
        if len(self.params.OUT)==0:
            print("No output for %s model" %self.params.name)
        else:
            print(15*'*')
            print("OUTPUT for model %s :" %self.params.name)
            c=1
            for i in self.params.OUT:
                print("OUT",c," : type = ",i.kind,"\tres =",i.res,"\tnormalization =",i.normalization,"\tloss =",i.loss)
                c=c+1

            print(15*'*')
        print()
        
        
    def delIN(self,num):
        print()
        if num <1 or num>len(self.params.IN):
            print("No Input deleted : Input number incorect with num =",num )
            print()

            return False
        i=self.params.IN[num-1]
        
        self.params.IN=self.params.IN[0:num-1]+self.params.IN[num:]
        print(15*"-")
        print("DELETED IN",num," :\ttype =",i.kind,"\tres =",i.res,"\tnormalization =",i.normalization)
        print(15*"-")
        print()
        return True

    def delOUT(self,num):
        print()
        if num <1 or num>len(self.params.OUT):
            print("No Output deleted : Output number incorect with num =",num )
            print()

            return False
        i=self.params.OUT[num-1]
        
        self.params.OUT=self.params.OUT[0:num-1]+self.params.OUT[num:]
        print(15*"-")
        print("DELETED OUT",num," :\ttype =",i.kind,"\tres =",i.res,"\tnormalization =",i.normalization,"\tloss =",i.loss)
        print(15*"-")
        print()
        return True
        
    def set_nn(self,Nconv,Nfilt,function,factiv=swish,model_type=False,Incertitude=False,end_layers=True,pen=1e-3):
       self.params.nn=NeuralStruct(Nconv,Nfilt,function,factiv=factiv,model_type=model_type,Incertitude=Incertitude,end_layers=end_layers,pen=pen)
    def set_trainparam(self,lr,epochs,batch_size,verbose=1,test_num=0,size_test=73,size_plage=15,use_scheduler=False):
       self.params.train_param=TrainParam(lr,epochs,batch_size,verbose,test_num,size_test,size_plage)
       
    def loadIN(self,num):
        
        if type(num)!=int:
            if num=="all":
                L=[]
                for i in self.params.IN:
                    d=np.load(i.path)
                    L.append(d)
                return L
            else:
                print("No input loaded : Input number incorect with num =",num )
                print()
                return False
            
            
        elif num<1 or num>len(self.params.IN):
            print("No input loaded : Input number incorect with num =",num )
            print()
            return False
        
        else:
            
            return np.load(self.params.IN[num-1].path)
        
    
    def loadOUT(self,num):
        
        if type(num)!=int:
            if num=="all":
                L=[]
                for i in self.params.OUT:
                    d=np.load(i.path)
                    L.append(d)
                return L
            else:
                print("No output loaded : Output number incorect with num =",num )
                print()
                return False
            
            
        elif num<1 or num>len(self.params.OUT):
            print("No ouput loaded : ouput number incorect with num =",num )
            print()
            return False
        
        else:
            
            return np.load(self.params.OUT[num-1].path)

    

    def split_train_test(self,ind,mode="time split"): 
        if mode =="time split":
            sizetest=self.params.train_param.size_test
            plage=self.params.train_param.size_plage
            test_num=self.params.train_param.test_num
            l=len(ind)
    
    
            if test_num==0:
                indtest=ind[0:sizetest]
                indtrain=ind[sizetest+plage:l-plage]
                
            elif test_num <l//sizetest-1:
                indtest=ind[sizetest*test_num:sizetest*(test_num+1)]
                indtrain=ind[0:sizetest*test_num-plage]+ind[sizetest*(test_num+1)+plage:l]
                
                
            elif test_num ==l//sizetest-1:
                indtest=ind[l-sizetest:l]
                indtrain=ind[plage:l-sizetest-plage]
            
            self.params.train_param.ind_train=indtrain
            self.params.train_param.ind_test=indtest
            return indtrain,indtest

        
        if mode =="1308":#année 2013 entière puis année 2008
            self.params.train_param.size_test=122
            self.params.train_param.size_plage=0
            l=len(ind)
    
    
            indtrain=[l for l in range(366)]
            indtest=[l for l in range(366,366+122)]

            
            self.params.train_param.ind_train=indtrain
            self.params.train_param.ind_test=indtest
            return indtrain,indtest
    def normalize(self,X,Y):
        
        self.params.scaler_in=[]
        self.params.scaler_out=[]

        Xft=[]
        Yft=[]
        for i in range(len(self.params.IN)):
            if self.params.IN[i].normalization=="MinMaxScaler":
                scaler=MinMaxScaler(X[i])[1]
                Xft.append(MinMaxScaler(X[i])[0])
                self.params.scaler_in.append(scaler)
        for i in range(len(self.params.OUT)):
            if self.params.OUT[i].normalization=="MinMaxScaler":
                scaler=MinMaxScaler(Y[i])[1]
                Yft.append(MinMaxScaler(Y[i])[0])
                self.params.scaler_out.append(scaler)
        return [Xft,Yft]
    def apply_normalization(self,X,Y):
        Xft=[]
        Yft=[]
        for i in range(len(self.params.IN)):
            if self.params.IN[i].normalization=="MinMaxScaler":
                Xft.append((X[i]-self.params.scaler_in[i]["min"])/(self.params.scaler_in[i]["max"]-self.params.scaler_in[i]["min"]))
        for i in range(len(self.params.OUT)):
            if self.params.OUT[i].normalization=="MinMaxScaler":
                Yft.append((Y[i]-self.params.scaler_out[i]["min"])/(self.params.scaler_out[i]["max"]-self.params.scaler_out[i]["min"]))

        return [Xft,Yft]
    def inv_normalize(self,Xft,Yft):
        X=[]
        Y=[]
        for i in range(len(self.params.IN)):
            
            if self.params.IN[i].normalization=="MinMaxScaler":
                X.append(inv_MinMaxScaler(Xft[i],self.params.scaler_in[i]))
        for i in range(len(self.params.OUT)):
            
            if self.params.OUT[i].normalization=="MinMaxScaler":
                Y.append(inv_MinMaxScaler(Yft[i],self.params.scaler_out[i]))
        return [X,Y]

    
    def my_model_general(self ,X):
        
        Nconv=self.params.nn.Nconv
        Nfilt=self.params.nn.Nfilt
        factiv= self.params.nn.factiv
        function=self.params.nn.function
        model_type=self.params.nn.model_type
        print(model_type)
        incertitude=self.params.nn.Incertitude
        # Définir les entrées du réseau:
        # [SSHR81train,SSTR27train]=DATA
       
        nb_boucle=len(X)-1
        all_input=[]
        ArchiOut = []
 
        upfactor = 3
        init       = "he_normal"  #'glorot_normal'+ #'orthogonal' #'normal'
        factout  = 'sigmoid'
        INssh=X[0]
        NL_,NC_=INssh[0].shape[0],INssh[0].shape[1]
        SSH=Input(shape=(NL_,NC_,1))
        all_input.append(SSH)
        ArchiA=[]

        for boucle in range(nb_boucle):
            INsst=X[boucle+1]
            
           
            NL_,NC_=INsst[0].shape[0],INsst[0].shape[1]
            SST=Input(shape=(NL_,NC_,1))
            all_input.append(SST)
        
           
            
            if boucle>=1:
                SSH=ArchiA
                
                
           
            #Détermination de la méthode d'upsampling
    

            
            
            if model_type=="RESACsub BN" or model_type=="RESACsub supBNsub":
                sub_layer = Lambda(lambda x:space_to_depth(x,3))
                sup_layer = Lambda(lambda x:depth_to_space(x,3))
                sub=sub_layer(SST)
                
            
                upsampled   = concatenate([SSH,SSH,SSH,SSH,SSH,SSH,SSH,SSH,SSH], axis=3) # CHANNEL LAST
                upsampled = concatenate([upsampled, upsampled], axis=3) 

                ArchiA=concatenate([upsampled,sub],axis=3)
       
         
            else:
                upsampled   = UpSampling2D((upfactor, upfactor), interpolation='bilinear')(SSH)
                ArchiA   = concatenate([upsampled, SST], axis=3) # CHANNEL LAST
                
                

 
                    
            if model_type=="RESACsub supBNsub" or model_type=="RESACsub BN":
                for i in np.arange(Nconv) : 
                    if model_type=="RESACsub supBNsub":
                        ArchiA = sup_layer(ArchiA)
                    ArchiA = BatchNormalization(axis=3)(ArchiA)
                    if model_type=="RESACsub supBNsub":
                        ArchiA = sub_layer(ArchiA)
                    
                    ArchiA = Conv2D(Nfilt,(3,3), activation=factiv, padding='same',kernel_initializer=init)(ArchiA)
                    ArchiA = Conv2D(Nfilt,(3,3), activation=factiv, padding='same',kernel_initializer=init)(ArchiA)
                    ArchiA = Conv2D(18,(3,3), activation=factiv, padding='same',kernel_initializer=init)(ArchiA)
                 
                    ArchiA = add([ArchiA,upsampled])
                    upsampled=ArchiA
                    ArchiA=concatenate([ArchiA,sub],axis=3)

            else :
                    
                for i in np.arange(Nconv) : #7
            
                    ArchiA = Conv2D(Nfilt,(3,3), activation=factiv, padding='same',kernel_initializer=init)(ArchiA)
                    ArchiA = Conv2D(Nfilt,(3,3), activation=factiv, padding='same',kernel_initializer=init)(ArchiA)
                    ArchiA = BatchNormalization(axis=3)(ArchiA)
            
            
            
            #Détermination de la fonction d'activation de sortie
            if function < 1:
              factout = 'linear' 
            elif 1 < function < 2 :
              factout = 'sig01'
            else : 
              factout = 'sigmoid'
                
            if model_type=="RESACsub supBNsub" or model_type=="RESACsub BN":
                ArchiA = Conv2D(9,(1,1), padding='same',kernel_initializer=init)(ArchiA)
                ArchiA = sup_layer(ArchiA)
                ArchiA = BatchNormalization(axis=3)(ArchiA) 
     
                ArchiA=Activation(factout,name="archi"+str(boucle+1))(ArchiA)
                ArchiOut.append(ArchiA)



            else:
                ArchiA = Conv2D(8,(3,3), activation=factiv, padding='same',kernel_initializer=init)(ArchiA)
                ArchiA = BatchNormalization(axis=3)(ArchiA)

                ArchiA = Conv2D(1,(1,1), activation=factout, padding='same',kernel_initializer=init,name='archi'+str(boucle+1))(ArchiA)
                ArchiOut.append(ArchiA)



        return Model(all_input, ArchiOut)
    
    
    def prepare_data(self,D):
        #prepare les données sous le bon format pour l'apprentissage
        [ind_train,ind_test]=[self.params.train_param.ind_train,self.params.train_param.ind_test]
        
        
        
        c=0
        for sc in self.params.scaler_in:
            print("IN Scaler:"+str(c),sc)
            
            c=c+1
        
        c=0
        for sc in self.params.scaler_out:
            print("OUT Scaler:"+str(c),sc)
            
            c=c+1
        
        # Définition des jeus de test et train
        x_train=[]
        x_test=[]
        y_train=[]
        y_test=[]
        
        reshape=True
        if reshape:
            for tab in D[0]:
                x_train.append(np.expand_dims(tab[ind_train],axis=3))
                x_test.append(np.expand_dims(tab[ind_test],axis=3))
    
            for tab in D[1]:
                y_train.append(np.expand_dims(tab[ind_train],axis=3))
                y_test.append(np.expand_dims(tab[ind_test],axis=3))
        
        else:
            for tab in D[0]:
                x_train.append(tab[ind_train])
                x_test.append(tab[ind_test])

            for tab in D[1]:
                y_train.append(tab[ind_train])
                y_test.append(tab[ind_test])
        print()
        print("IN LEN : __________________")
        print(len(x_train))
        print(len(y_train))
        print(len(x_test))
        print(len(y_test))
        
        print("IN SHAPE : __________________")
        print()
        
        print("Xtrain shape:")
        for k in x_train:
            print(k.shape)
        print()
        print("Ytrain shape:")
        for k in y_train:
            print(k.shape)
        print()
        print("Xtest shape:")
        for k in x_test:
            print(k.shape)
        print()
        print("Ytest shape:")
        for k in y_test:
            print(k.shape)
        print()

        return x_train,y_train,x_test,y_test

        
    def compile_with(self,x_train):
            
      
        
        
        lr=self.params.train_param.lr
        batch_size=self.params.train_param.batch_size
        verbose=self.params.train_param.verbose
        epochs=self.params.train_param.epochs
    
        
        # Définition des jeus de test et train
        
        loss=[]
       
        for k in range(len(self.params.OUT)):
            if self.params.OUT[k].loss=="mse":
                loss.append(tensorflow.keras.losses.MeanSquaredError())
            if self.params.OUT[k].loss=="loss incertitude":
                loss.append(loss_mle_theo)
        
        def get_lr_metric(optimizer):
            def lr(y_true, y_pred):
                return optimizer._decayed_lr(tensorflow.float32) # I use ._decayed_lr method instead of .lr
            return lr


     
        optimizer = optimizers.Adam(learning_rate=2*(10**lr))
        lr_metric = get_lr_metric(optimizer)
        self.model.compile(loss=loss,optimizer=optimizer,metrics=[realrmse2008,lr_metric])#,loss_weights=[0.7,0.3]
        self.model.summary()
 
        
        
    def fit_with(self,x_train,y_train,x_test,y_test,sc="normal"):
            
      
        
        
        lr=self.params.train_param.lr
        batch_size=self.params.train_param.batch_size
        verbose=self.params.train_param.verbose
        epochs=self.params.train_param.epochs
    
           
        if self.params.train_param.use_scheduler:
            print("USE SCHEDULER")
            

            callback = LearningRateScheduler(self.define_scheduler(sc))
            
            H = self.model.fit(x_train, y_train, epochs=epochs,batch_size=batch_size,shuffle=True, verbose=verbose,callbacks=callback, validation_data=(x_test, y_test),workers=-1,use_multiprocessing=True)
        else:
            H = self.model.fit(x_train, y_train, epochs=epochs,batch_size=batch_size,shuffle=True, verbose=verbose, validation_data=(x_test, y_test),workers=-1,use_multiprocessing=True)
         
        self.H=H

                        
    def print_result(self,NUM=[15],save_fig=True,path_save="saved_figures/"):
        
        
        Incertitude=self.params.nn.Incertitude #TO DO mettre da,s la structure
        if save_fig:
            if self.params.path_save=="not defined":
                print("'%s' has no path_save configured : can not save figures"%self.params.name) 
                save_fig=False
            else:
                if not os.path.exists(self.params.path_save+"/figures"):
                    os.makedirs(self.params.path_save+"/figures")
                path_save=self.params.path_save+"/figures/"
        X=self.loadIN("all")
        Y=self.loadOUT("all")
        Xft,Yft=self.normalize(X,Y)
        # Y=np.expand_dims(Y, axis=3)
 
        def real_rmse(loss,scaler):
            return(loss**(1/2))*(scaler["max"]-scaler["min"])
        

        [ind_train,ind_test]=[self.params.train_param.ind_train,self.params.train_param.ind_test]
 
        # Définition des jeus de test et train
        x_train=[]
        x_valid=[]
        y_train=[]
        y_valid=[]
        xtot=[]
        ytot=[]
        for tab in Xft:
            x_train.append(np.expand_dims(tab[ind_train],axis=3))
            x_valid.append(np.expand_dims(tab[ind_test],axis=3))
            xtot.append(np.array(np.expand_dims(tab,axis=3)))
       
    
        for tab in Yft:
            y_train.append(np.expand_dims(tab[ind_train],axis=3))
            y_valid.append(np.expand_dims(tab[ind_test],axis=3))
            ytot.append(np.expand_dims(tab,axis=3))
            
        print(xtot[0].shape)
        print(x_train[0].shape)
        print(x_valid[0].shape)
        print(ytot[0].shape)
        print(y_train[0].shape)
        print(y_valid[0].shape)
        
        if Incertitude:
            pred=[]
            pred_train=[]
            prediction=[]
            pred1=self.model.predict(x_valid)
            pred_train1=self.model.predict(x_train)
            prediction1=self.model.predict(xtot)
            print(pred_train1[0].shape)
            for k in range (len(ytot)):
                pred.append(pred1[k][:,:,:,0])
                print(pred1[k][:,:,:,0].shape)

                pred_train.append(pred_train1[k][:,:,:,0])
                prediction.append(prediction1[k][:,:,:,0])

            
               
        else:
            pred=self.model.predict(x_valid)
            pred_train=self.model.predict(x_train)
            prediction=self.model.predict(xtot)
        
        
        x_train,pred_train=self.inv_normalize(x_train, pred_train)
        x_train,y_train=self.inv_normalize(x_train, y_train)
        
        x_valid,pred=self.inv_normalize(x_valid, pred)
        x_valid,y_valid=self.inv_normalize(x_valid, y_valid)

        xtot,ytot=self.inv_normalize( xtot,ytot)
        xtot,prediction=self.inv_normalize( xtot,prediction)

        
        title=self.params.name
        H=self.H
        
        plt.figure(figsize=(35,20))
        plt.title("Courbe apprentissage du modèle "+title)
        plt.plot(H.history["loss"])
        plt.plot(H.history["val_loss"])
        plt.legend(["loss on train","val_loss on test"])
        plt.yscale("log")
        plt.grid(True, which="both", linestyle="--")
        plt.xlabel("epoch")
        if save_fig:
            plt.savefig(path_save+"Courbe apprentissage "+title+".png")
        plt.figure(figsize=(35,20))
        plt.title("Courbe MSE de sortie du modèle "+title)
        plt.plot(H.history["archi2_loss"])
        plt.plot(H.history["val_archi2_loss"])
        plt.legend(["loss on train","val_loss on test"])
        plt.yscale("log")
        plt.grid(True, which="both", linestyle="--")
        plt.xlabel("epoch")
        if save_fig:
            plt.savefig(path_save+"Courbe MSE sortie "+title+".png")
            
            
        if type(NUM)==list:
            for l in range(len(self.params.OUT)):

                for num in NUM:
                    title=self.params.name+" "+self.params.OUT[l].kind+" "+self.params.OUT[l].res
                    fig,axes=plt.subplots(nrows=1,ncols=2,figsize=(35,20))
                        
                    im1=axes[0].imshow(pred[l][num])
                    clim1=im1.properties()["clim"]
                    # im_ratio = pred[num].shape[0]/pred[num].shape[1] 
                    # plt.colorbar(im,fraction=0.046*im_ratio, pad=0.04)
        
                    im2=axes[1].imshow(y_valid[l][num])
                    clim2=im2.properties()["clim"]
                    
                    clim=(min(clim1[0],clim2[0]),max(clim1[1],clim2[1]))
                    im1=axes[0].imshow(pred[l][num],clim=clim)
                    im2=axes[1].imshow(y_valid[l][num],clim=clim)            
                    axes[0].set_title(title+" prédiction")
                    axes[1].set_title(title+" true")
        
                    
                    fig.colorbar(im1, ax=axes.ravel().tolist(), shrink=0.5)
                    fig.show()
                    if save_fig:
                        plt.savefig(path_save+title+"prediction.png")
                    plt.figure(figsize=(35,20))
                    plt.imshow(y_valid[l][num]-np.reshape(pred[l][num], y_valid[l][num].shape),cmap="coolwarm")
                    plt.colorbar()
        
                    plt.title(title+ " différence")
                    
                    if save_fig:
                        plt.savefig(path_save+"Difference "+title+".png")
                        
                        
                        
                        
                        
                        
        
                    title=self.params.name+" "+self.params.OUT[l].kind+" "+self.params.OUT[l].res
                    fig,axes=plt.subplots(nrows=1,ncols=2,figsize=(35,20))
                        
                    im1=axes[0].imshow(pred_train[l][num])
                    clim1=im1.properties()["clim"]
                    # im_ratio = pred[num].shape[0]/pred[num].shape[1] 
                    # plt.colorbar(im,fraction=0.046*im_ratio, pad=0.04)
        
                    im2=axes[1].imshow(y_train[l][num])
                    clim2=im2.properties()["clim"]
                    
                    clim=(min(clim1[0],clim2[0]),max(clim1[1],clim2[1]))
                    im1=axes[0].imshow(pred_train[l][num],clim=clim)
                    im2=axes[1].imshow(y_train[l][num],clim=clim)            
                    axes[0].set_title(title+" prédiction")
                    axes[1].set_title(title+" true")
        
                    
                    fig.colorbar(im1, ax=axes.ravel().tolist(), shrink=0.5)
                    fig.show()
                    if save_fig:
                        plt.savefig(path_save+title+"prediction.png")
                    plt.figure(figsize=(35,20))
                    plt.imshow(y_train[l][num]-np.reshape(pred_train[l][num], y_train[l][num].shape),cmap="coolwarm")
                    plt.colorbar()
        
                    plt.title(title+ " différence")
                    
                    if save_fig:
                        plt.savefig(path_save+"Difference "+title+".png")
                        
                        
                        
                    x=np.hstack(y_valid[l][num])
                    y=np.hstack(np.reshape(pred[l][num], y_valid[l][num].shape))
                    plt.figure(figsize=(35,20))
                    plt.subplot(121)
                    plt.title(title +" scatter plot prédiction / vérité")
            
                    plt.scatter(x, y,c = 'blue', marker = 'x')
                    plt.plot(x,x,c="red")
                    
                    h=x-y
                    plt.subplot(122,)
                    plt.hist(h,bins=25)
                    plt.title(title +" histogramme des erreurs")
                    if save_fig:
                        plt.savefig(path_save+"Histogramme des erreurs "+title+".png")
                plt.figure(figsize=(35,20))    
                err_train=[]
                err_valid=[]
                err_reste=[]
                ind_reste=[]
                
                mse = tensorflow.keras.losses.MeanSquaredError()

                for k in range (len(xtot[0])):
                    
                        
                    if k in ind_train:
                        err_train.append(math.sqrt(mse(ytot[l][k],prediction[l][k]).numpy()))
                    elif k in ind_test:
                        err_valid.append(math.sqrt(mse(ytot[l][k],prediction[l][k]).numpy()))
                    else:
                        ind_reste.append(k)
                        err_reste.append(math.sqrt(mse(ytot[l][k],prediction[l][k]).numpy()))

                        
     
    
                plt.scatter(ind_train, err_train ,c="blue",marker="+")
                plt.scatter(ind_test, err_valid ,c="red",marker="+")
                plt.scatter(ind_reste,  err_reste,c="orange",marker="+")
                
                plt.title(title+" MSE selon la période de l'année")
                plt.legend(["Jeu de train","Jeu de test","Données exclues"])
                plt.ylabel("RMSE")
                plt.xlabel("Jours")
                plt.show(block=False)
                if save_fig:
                        plt.savefig(path_save+"RMSE selon la période de l'année "+title+".png")    
    def add_endlayers(self):#if you want to implement denoising netwok derectly on top of the previous model
        init       = "he_normal"  #'glorot_normal'+ #'orthogonal' #'normal'
        for layer in self.model.layers:
            if "batch_normalization" not in layer.name:
                layer.trainable=False
            else:
                layer.trainable=False
                print("BN")
        self.model.layers[-1]._name="previous_ouput"
        out=self.model.output[-1]
       
      
        ArchiA = Conv2D(64,(7,7), padding='same',activation=swish,kernel_initializer=init)(out)
        ArchiA = Conv2D(64,(7,7), padding='same',activation=swish,kernel_initializer=init)(ArchiA)

        ArchiA = Conv2D(1,(7,7), padding='same',activation=swish,kernel_initializer=init,name="archi3")(ArchiA)
        self.model=Model(self.model.inputs,[self.model.outputs[0],self.model.outputs[1],ArchiA])
        

        
        
    def save_model(self,mod="json"):
        if self.params.path_save=="not defined":
            raise ValueError("'%s' has no path_save configured : can not save figures"%self.params.name) 
        else:
            if not os.path.exists(self.params.path_save+"/architecture"):
                os.makedirs(self.params.path_save+"/architecture")
            path_save=self.params.path_save+"/architecture/"
        if mod=="json":
            model_json = self.model.to_json()
            with open(path_save+"model.json", "w") as json_file:
                json_file.write(model_json)
            # serialize weights to HDF5
            self.model.save_weights(path_save+"model.h5")
            print("Saved as a json file in :"+path_save)
        else:
            self.model.save(path_save+"model")
        np.save(path_save+"history",self.H.history)

        with open(path_save+"params.pickle", 'wb') as file:
            pickle.dump(self.params, file) 
    




    def load_model(self,path="not defined",mod="json"):
        
        if path=="not defined":
            if self.params.path_save=="not defined":
                raise ValueError("'%s' has no path_save configured : can not save figures"%self.params.name) 
            else:
                l=os.listdir(self.params.path_save+"/architecture/")
                if os.path.exists(self.params.path_save+"/architecture/"+"model.json"):
                    # print("SUUUUi")
                    
                    if mod=="json":
                        json_file = open(self.params.path_save+"/architecture/"+"model.json", 'r')
                        loaded_model_json = json_file.read()
                        json_file.close()
                        loaded_model = model_from_json(loaded_model_json)
                        # load weights into new model
                        loaded_model.load_weights(self.params.path_save+"/architecture/"+"model.h5")
                        print("Loaded model json file at : "+self.params.path_save+"/architecture/")
                        self.model=loaded_model
                    else:
                        
                        self.model=load_model(self.params.path_save+"/architecture/"+"model")
                if os.path.exists(self.params.path_save+"/architecture/"+"history.npy"):
                    self.H=np.load(self.params.path_save+"/architecture/"+"history.npy",allow_pickle=True).item()

                with open(self.params.path_save+"/architecture/"+"params.pickle", 'rb') as file:
                    self.params = pickle.load(file)
                    
                 
                    
                    
                    
                    
                    
    def load_history(self,path="not defined"):
        
        if path=="not defined":
            if self.params.path_save=="not defined":
                raise ValueError("'%s' has no path_save configured : can not save figures"%self.params.name) 
            else:
                if os.path.exists(self.params.path_save+"/architecture/"+"history.npy"):
                    self.H=np.load(self.params.path_save+"/architecture/"+"history.npy",allow_pickle=True).item()
            return self.H
                
    def getpath(self):
        l=os.getcwd()
        
        if l[6]=="p":
           
            self.params.path_save=PATH_SAVE+"/"+self.params.name
            
       
            path_data=PATH_DATA
            
            self.params.path_data=path_data
        
        
        else:
            path_save=PATH+"\\Save"
            path_save=path_save.replace("\\", "/")
            self.params.path_save=path_save+"/"+self.params.name
            
            path_data=PATH+"\\Data"
            path_data=path_data.replace("\\", "/")
            
            self.params.path_data=path_data+"/"
    
    
    def convert_model_from_gpu(self):
        
        PATH="/home/pequan2/archambault/local"
        path_save=PATH+"\\Save"
        path_save=path_save.replace("\\", "/")
        self.params.path_save=path_save
        
        path_data=PATH+"\\Data\\NATL60byVar"
        path_data=path_data.replace("\\", "/")
        old_d_path= self.params.path_data


        self.params.path_data=path_data
        for d in self.params.IN:
            d.path=d.path.replace(old_d_path,self.params.path_data)
        for d in self.params.OUT:
            d.path=d.path.replace(old_d_path,self.params.path_data)

    def define_scheduler(self,sc):
        
        
        def scheduler(epoch, lr):
          if epoch < 20:
            return lr
          elif epoch<60:
              return lr * tensorflow.math.exp(-0.02)
          else:
            return lr * tensorflow.math.exp(-0.05)
        def scheduler2(epoch, lr):
          if epoch < 5:
            return lr
          elif epoch<20:
              return lr * tensorflow.math.exp(-0.2)
          else:
            return lr * tensorflow.math.exp(-0.5)
        
        def schedulerRESACsub_BN(epoch, lr):
          if epoch < 20:
            return lr
          elif epoch<60:
              return lr * tensorflow.math.exp(-0.02)
          else:
            return lr * tensorflow.math.exp(-0.05)
        def schedulerRESACsub_supBNsub(epoch, lr):
          if epoch < 20:
            return lr
          elif epoch<60:
              return lr * tensorflow.math.exp(-0.02)
          else:
            return lr * tensorflow.math.exp(-0.05)
        def schedulerRESAC(epoch, lr):
          if epoch < 20:
            return lr
          elif epoch<60:
              return lr * tensorflow.math.exp(-0.02)
          else:
            return lr * tensorflow.math.exp(-0.05)
      
        
        def schedulerendlayers(epoch, lr):
          if epoch < 10:
            return lr
          elif epoch<30:
              return lr * tensorflow.math.exp(-0.02)
          elif epoch<50:
              return lr * tensorflow.math.exp(-0.04)
          elif epoch<70:
              return lr * tensorflow.math.exp(-0.08)
          else:
            return lr * tensorflow.math.exp(-0.12)
        if sc=="normal":
            return scheduler
        if sc=="RESACsub BN":
            return schedulerRESACsub_BN
        if sc=="RESACsub supBNsub":
            return schedulerRESACsub_supBNsub
        if sc=="RESAC":
            return schedulerRESAC
        if sc=="endlayers":
            return schedulerendlayers
