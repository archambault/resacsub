#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 10:51:37 2022

@author: archambault
"""
import matplotlib.pyplot as plt
import numpy as np
from metrics import *
from train import *
from architecture import *
from path_manager import *


    
def plot_line(Images,Titres,cmap,save_name,label="SSH(m)",shrink=0.3,center_colormap=True):
    #this function plots a line of n images
    fig,axes=plt.subplots(nrows=1,ncols=len(Images),figsize=(35,15))
    for ax in axes:
        ax.set_xticks([])
        ax.set_yticks([])
    clim=(100,-100)
    for n in range (len(Images)):
        im=axes[0].imshow(Images[n],cmap=cmap)
        clim_new=im.properties()["clim"]
        clim=(min(clim[0],clim_new[0]),max(clim[1],clim_new[1]))
    if center_colormap:
        clim=(-max(abs(clim[0]),abs(clim[1])),max(abs(clim[0]),abs(clim[1])))

    for n in range (len(Images)):
        im=axes[0].imshow(Images[n],cmap=cmap,clim=clim)
        
    for n in range (len(Images)):
  
        axes[n].imshow(Images[n],clim=clim,cmap=cmap)
        axes[n].set_title(Titres[n],fontsize=20)

    col=fig.colorbar(im,ax= axes[:], location ="right",shrink=shrink)
    col.ax.tick_params(labelsize=20)
    col.set_label(label=label,size=20)
    
    plt.savefig(save_name+".pdf",bbox_inches="tight")
    
    

def compare_n_model(list_model, NAME,mode=["mean"],n_loss="loss",n_val="val_loss",save_fig=True):
        # scaler={'min': -0.6403334140777588, 'max': 1.3279966115951538}
    scaler={'min': -0.856661319732666, 'max': 1.3510133028030396}

    LOSS = []
    VAL_LOSS = []

    path_save = PATH_SAVE

    for model in list_model:
        loss = []
        val_loss = []
        for name in model:
            

            model_arch = modelStruct(name)
            model_arch.params.path_save = path_save + "/" + model_arch.params.name
            print(path_save + "/" + model_arch.params.name)
            model_arch.load_history()
            print(model_arch.H)
            # if count==0:
            #     model_arch.load_model()
            #     print(model_arch.model)
            #     print(model_arch.model.summary())
            #     count=1

            l=[]
            for k in model_arch.H.keys():
                l.append(k)
            print(l)
            # x=input()
            # print(len(model_arch.H["loss"]))
            loss.append(model_arch.H[n_loss])
            # print(model_arch.H)

            val_loss.append(model_arch.H[n_val])
        LOSS.append(loss)
        VAL_LOSS.append(val_loss)
        # model_arch.load_model()
        # model_arch.model.summary()
    # return(model_arch.H)

    epoch = list(range(len(LOSS[0][0])))

    title = NAME[0]
    for n in NAME[1:]:
        title = title + " VS "+n
    color = [["blue", "royalblue"], ["red", "lightcoral"],["green", "lightgreen"],["darkviolet", "mediumorchid"],["orange", "goldenrod"]]


    if "mean" in mode:
        val_loss_mean=[]
        loss_mean=[]
        plt.figure(figsize=(35,20))
        plt.title("Mean loss model : " + title)
    
        LEGEND=[]
        
        for i in range(len(NAME)):
    
            plt.plot(epoch, np.mean(np.array(LOSS[i]), axis=0), "o-", color=color[i][0])
            plt.plot(epoch, np.mean(np.array(VAL_LOSS[i]), axis=0), "o-", color=color[i][1])
            
            LEGEND.append("loss "+NAME[i])
            LEGEND.append("val_loss "+NAME[i])
    
    
        plt.yscale("log")
        plt.legend(LEGEND)
        plt.grid(True, which="both", linestyle="--")
        plt.xlabel("epoch")
        if save_fig:
            plt.savefig("Mean_loss_model" + title+".png")
        
    if "std" in mode:

        plt.figure(figsize=(35,20))
        plt.title("Std loss model : " + title)

        LEGEND=[]
        
        for i in range(len(NAME)):
            plt.plot(epoch, np.std(np.array(LOSS[i]), axis=0), "o-", color=color[i][0])
            plt.plot(epoch, np.std(np.array(VAL_LOSS[i]), axis=0), "o-", color=color[i][1])
            LEGEND.append("std loss "+NAME[i])
            LEGEND.append("std val_loss "+NAME[i])
    
    
        plt.yscale("log")
        plt.legend(LEGEND)
    
        plt.grid(True, which="both", linestyle="--")
        plt.xlabel("epoch")
        
    
        if save_fig:
            plt.savefig("Std_loss_model" + title+".png")
            
    if "median" in mode:

        plt.figure(figsize=(35,20))
        plt.title("Median loss model : " + title)

        LEGEND=[]
        
        for i in range(len(NAME)):
            plt.plot(epoch, np.median(np.array(LOSS[i]), axis=0), "o-", color=color[i][0])
            plt.plot(epoch, np.median(np.array(VAL_LOSS[i]), axis=0), "o-", color=color[i][1])
            LEGEND.append("Median loss "+NAME[i])
            LEGEND.append("Median val_loss "+NAME[i])
            val_loss_mean.append(real_rmse(np.mean(np.median(np.array(VAL_LOSS[i]), axis=0)[70:90]),scaler))
            loss_mean.append(real_rmse(np.mean(np.mean(np.array(LOSS[i]), axis=0)[70:90]),scaler))
    
    
        plt.yscale("log")
        plt.legend(LEGEND)
    
        plt.grid(True, which="both", linestyle="--")
        plt.xlabel("epoch")
        
    
        if save_fig:
            plt.savefig("Median_loss_model" + title+".png")
        return(loss_mean,val_loss_mean)
    
#compare_n_model([["RESAC"]], ["1"],mode=["mean"],n_loss="lr",n_val="lr",save_fig=False)
#compare_n_model([["RESACsub supBNsub"]], ["2"],mode=["mean"],n_loss="lr",n_val="lr",save_fig=False)
#compare_n_model([["RESACsub BN"]], ["3"],mode=["mean"],n_loss="lr",n_val="lr",save_fig=False)
#compare_n_model([["Denoising network"]], ["4"],mode=["mean"],n_loss="lr",n_val="lr",save_fig=False)
