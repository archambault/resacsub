#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 16:24:03 2022

@author: archambault
"""


global PATH_HOME
global PATH_DATA
global PATH_SAVE

PATH_HOME="/home/pequan2/archambault/local"
PATH_DATA=PATH_HOME+"/Data2"
PATH_SAVE=PATH_HOME+"/Save2"