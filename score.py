
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 15:57:25 2021

@author: archambault
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 10:59:48 2021

@author: archambault
"""



import math
from   matplotlib  import cm
import sys
from   time  import time
import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import random 
from sklearn.preprocessing import StandardScaler
import pickle

from tensorflow.keras.layers import Input, Dense, Flatten, Reshape, AveragePooling2D, Dropout, add 
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D #, Deconvolution2D
from tensorflow.keras.layers import Concatenate, concatenate, BatchNormalization,Lambda ,Activation
from tensorflow.keras.models    import Model
from tensorflow.keras.models    import load_model, model_from_json
import tensorflow.keras.callbacks
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras import regularizers
from tensorflow.keras import optimizers
from functools import partial
# from tensorflow_addons.metrics import RQsquare
# from tensorflow.keras.utils import plot_model
# from bayes_opt import BayesianOptimization  #Biblio pour l'optimisation bayesienne
# from bayes_opt.logger import JSONLogger     
# from bayes_opt.event import Events
# from bayes_opt.util import load_logs        #Pour charger les données à l'issu de l'optimisation 
from tensorflow.nn import swish
from tensorflow.nn import depth_to_space
from tensorflow.nn import space_to_depth
from tensorflow.keras.callbacks import LearningRateScheduler

from architecture import *
from train import *
from path_manager import *
from metrics import *





def load_noised_model(n_model,path="EXP10pp/EPX10Subpix"):
    
    
    model_arch=reload_model(path+str(n_model))

    model_arch.convert_model_from_gpu()
    
    X=model_arch.loadIN("all")
    Y=model_arch.loadOUT("all")
    X,Y=model_arch.normalize(X,Y)
    
    
    
    [ind_train,ind_test]=[model_arch.params.train_param.ind_train,model_arch.params.train_param.ind_test]
    
    # Définition des jeus de test et train
    x_train=[]
    x_valid=[]
    y_train=[]
    y_valid=[]
    
    for tab in X:
        x_train.append(tab[ind_train])
        x_valid.append(tab[ind_test])
    
    for tab in Y:
        y_train.append((np.expand_dims(tab[ind_train], -1)))
        y_valid.append(np.expand_dims(tab[ind_test], -1))
    
    pred_train=model_arch.model.predict(x_train)[-1]
    pred_valid=model_arch.model.predict(x_valid)[-1]
    y_train=y_train[-1]
    y_valid=y_valid[-1]
    
    weights=[1 for i in range(366)]
    if False:
        Najout=30
    
        PER=[100/Najout*i for i in range(Najout)]
        # print(percentile)
        perc_x=np.array([np.percentile(pred_train,percentile) for percentile in PER ])
        perc_y=np.array([np.percentile(y_train,percentile) for percentile in PER ])
        
        
        for k in range(perc_x.shape[0]):
       
            
            Nmult=1
            mat=np.expand_dims(np.ones((Nmult,y_train.shape[1],y_train.shape[2]))*perc_x[k],3)
            pred_train=np.concatenate((pred_train,mat),axis=0)
            mat=np.expand_dims(np.ones((Nmult,y_train.shape[1],y_train.shape[2]))*perc_y[k],3)
            y_train=np.concatenate((y_train,mat),axis=0)
    
    
        w=5
        weights=weights +[w for i in range(Najout)]
        print(weights)
    
    
    score=np.mean(realrmse2008(y_valid, pred_valid).numpy())
    return [x_train,y_train,pred_train], [x_valid,y_valid,pred_valid],score


def load_denoising_model(PATH):
     json_file = open(PATH+"/model.json", 'r')
     loaded_model_json = json_file.read()
     json_file.close()
     loaded_model = model_from_json(loaded_model_json)
     # load weights into new model
     loaded_model.load_weights(PATH+"/model.h5")
     return loaded_model
     
     
    
def load_denoising_history(PATH):
    return np.load(PATH+"/history.npy",allow_pickle=True).item()

    

def inv_normR03(X):
    MAX=1.3550180196762085
    MIN=-0.8636236190795898
    return (MAX-MIN)*X+MIN
# model_arch=reload_model("EXP10pp/EPX10Subpix0")







def score_model(name):
     
    model_arch=reload_model(name)
    model_arch.convert_model_from_gpu()
    
    X=model_arch.loadIN("all")
    Y=model_arch.loadOUT("all")
    X,Y=model_arch.normalize(X,Y)
    
    
    
    [ind_train,ind_test]=[model_arch.params.train_param.ind_train,model_arch.params.train_param.ind_test]
    
    # Définition des jeus de test et train
    x_train=[]
    x_valid=[]
    y_train=[]
    y_valid=[]
    
    for tab in X:
        x_train.append(tab[ind_train])
        x_valid.append(tab[ind_test])
    
    for tab in Y:
        y_train.append((np.expand_dims(tab[ind_train], -1)))
        y_valid.append(np.expand_dims(tab[ind_test], -1))
    
    pred_train=model_arch.model.predict(x_train)[-1]
    pred_valid=model_arch.model.predict(x_valid)[-1]
    y_train=y_train[-1]
    y_valid=y_valid[-1]
    
    rmse=realrmse2008(pred_valid,y_valid).numpy()
    croprmse=realrmse2008_crop(pred_valid,y_valid)
    rmse10,rmse90=realrmse2008_decile(pred_valid,y_valid)
      
    
    rmse=np.array(rmse)
    croprmse=np.array(croprmse)
    rmse10=np.array(rmse10)
    rmse90=np.array(rmse90)
    
    
   
    print("SCORES:")
    print("rmse : ",np.mean(rmse))
    print("rmse crop : ",np.mean(croprmse))
    print("rmse10 : ",np.mean(rmse10))
    print("rmse90 : ",np.mean(rmse90))
    print()
    
    
    
    return np.mean(rmse),np.mean(croprmse),np.mean(rmse10),np.mean(rmse90)

def score_denoising_model(name_model,name_denoising_model):#non testé
     
    model_arch=reload_model(name_model)
    # model_arch.convert_model_from_gpu()
    model_arch.convert_model_from_gpu()

    X=model_arch.loadIN("all")
    Y=model_arch.loadOUT("all")
    X,Y=model_arch.normalize(X,Y)
    
    
    
    [ind_train,ind_test]=[model_arch.params.train_param.ind_train,model_arch.params.train_param.ind_test]
    
    # Définition des jeus de test et train
    x_train=[]
    x_valid=[]
    y_train=[]
    y_valid=[]
    
    for tab in X:
        x_train.append(tab[ind_train])
        x_valid.append(tab[ind_test])
    
    for tab in Y:
        y_train.append((np.expand_dims(tab[ind_train], -1)))
        y_valid.append(np.expand_dims(tab[ind_test], -1))
    
    pred_train=model_arch.model.predict(x_train)[-1]
    pred_valid=model_arch.model.predict(x_valid)[-1]
    y_train=y_train[-1]
    y_valid=y_valid[-1]
    
    
    mod=load_denoising_model(PATH_SAVE+"/"+name_denoising_model)
    mod.summary()
    y_final_valid=mod.predict(pred_valid)
    
    
    rmse=realrmse2008(pred_valid,y_valid).numpy()
    croprmse=realrmse2008_crop(pred_valid,y_valid)
    rmse10,rmse90=realrmse2008_decile(pred_valid,y_valid)
      
    
    rmse=np.array(rmse)
    croprmse=np.array(croprmse)
    rmse10=np.array(rmse10)
    rmse90=np.array(rmse90)
    
    
   
    print("SCORE model:")
    print("rmse : ",np.mean(rmse))
    print("rmse crop : ",np.mean(croprmse))
    print("rmse10 : ",np.mean(rmse10))
    print("rmse90 : ",np.mean(rmse90))
    print()
     
    rmse=realrmse2008(y_final_valid,y_valid).numpy()
    croprmse=realrmse2008_crop(y_final_valid,y_valid)
    rmse10,rmse90=realrmse2008_decile(y_final_valid,y_valid)
      
    
    rmse=np.array(rmse)
    croprmse=np.array(croprmse)
    rmse10=np.array(rmse10)
    rmse90=np.array(rmse90)
    
    print("SCORE denoised model:")
    print("rmse : ",np.mean(rmse))
    print("rmse crop : ",np.mean(croprmse))
    print("rmse10 : ",np.mean(rmse10))
    print("rmse90 : ",np.mean(rmse90))
    print()
    return np.mean(rmse),np.mean(croprmse),np.mean(rmse10),np.mean(rmse90)

# score_model("RESACsub supBNsub")
# score_model("RESACsub BN")
# score_model("RESAC")
#score_denoising_model("RESACsub supBNsub","Denoising network")

