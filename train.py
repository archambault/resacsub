#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 15:57:25 2021

@author: archambault
"""

from architecture import *
from metrics import *
from path_manager import *


    
def reload_model(name,seed=False):
    random.seed(seed)
    if seed!=False:
        model_arch=modelStruct(name+str(seed))
    else: 
        model_arch=modelStruct(name)
    
    path_save=PATH_SAVE
    path_data=PATH_DATA
    
    model_arch.params.path_save=path_save+"/"+model_arch.params.name
    
    model_arch.load_model()

    model_arch.printIN()
    model_arch.printOUT()
    
    # print(model_arch.model)
    model_arch.model.summary()

    # model_arch.print_result()
    return model_arch


def reload_denoising_model(PATH):
     json_file = open(PATH_SAVE+"/"+PATH+"/model.json", 'r')
     loaded_model_json = json_file.read()
     json_file.close()
     loaded_model = model_from_json(loaded_model_json)
     # load weights into new model
     loaded_model.load_weights(PATH_SAVE+"/"+PATH+"/model.h5")
     return loaded_model
     
     
    
def train_RESACsub_BN(name):
    
    model_type="RESACsub BN"
    Nconv=5
    Nfilt=32
    epochs=150
    lr=-3
    
    batch_size=16
    test_num=0
    Incertitude=False
    end_layers=False
    pen=1e-3
    model_arch=modelStruct(name)
    model_arch.getpath()
    path_data=model_arch.params.path_data

    
    model_arch.addIN("SSH","R81","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R81.npy")
    model_arch.addIN("SST","R27","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R27.npy")
    model_arch.addIN("SST","R09","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R09.npy")
    model_arch.addIN("SST","R03","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R03.npy")


    
    if Incertitude:
        loss="loss incertitude"
    else:
        loss="mse"
    
    model_arch.addOUT("SSH","R27","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R27.npy",loss)
    model_arch.addOUT("SSH","R09","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R09.npy",loss)
    model_arch.addOUT("SSH","R03","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R03.npy",loss)


    
    model_arch.printIN()
    model_arch.printOUT()
    
    
   

    model_arch.set_nn(Nconv=Nconv, Nfilt=Nfilt, function=0.1,model_type=model_type,Incertitude=Incertitude,end_layers=end_layers,pen=pen)
    model_arch.set_trainparam(lr=lr, epochs=epochs, batch_size=batch_size,test_num=test_num,use_scheduler=True)
    model_arch.params.train_param.use_scheduler=True

    l=366+122
    model_arch.split_train_test(list(np.arange(l)),mode="1308")
    x_train,y_train,x_test,y_test=model_arch.prepare_data(model_arch.normalize(model_arch.loadIN("all"), model_arch.loadOUT("all")))
    model_arch.model=model_arch.my_model_general(x_train)

    model_arch.compile_with(x_train)

    model_arch.fit_with(x_train,y_train,x_test,y_test,sc=model_type)
    model_arch.save_model() 

    # model_arch.print_result()
    return model_arch

   
def train_RESACsub_supBNsub(name):

    
    model_type="RESACsub supBNsub"
    Nconv=5
    Nfilt=32
    epochs=150
    lr=-3
    batch_size=16
    
    test_num=0
    Incertitude=False
    end_layers=False
    pen=1e-3

    model_arch=modelStruct(name)
    model_arch.getpath()
    path_data=model_arch.params.path_data

    
    model_arch.addIN("SSH","R81","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R81.npy")
    model_arch.addIN("SST","R27","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R27.npy")
    model_arch.addIN("SST","R09","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R09.npy")
    model_arch.addIN("SST","R03","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R03.npy")


    
    if Incertitude:
        loss="loss incertitude"
    else:
        loss="mse"
    
    model_arch.addOUT("SSH","R27","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R27.npy",loss)
    model_arch.addOUT("SSH","R09","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R09.npy",loss)
    model_arch.addOUT("SSH","R03","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R03.npy",loss)


    
    model_arch.printIN()
    model_arch.printOUT()
    
    
   

    model_arch.set_nn(Nconv=Nconv, Nfilt=Nfilt, function=0.1,model_type=model_type,Incertitude=Incertitude,end_layers=end_layers,pen=pen)
    model_arch.set_trainparam(lr=lr, epochs=epochs, batch_size=batch_size,test_num=test_num,use_scheduler=True)

    l=366+122
    model_arch.split_train_test(list(np.arange(l)),mode="1308")
    x_train,y_train,x_test,y_test=model_arch.prepare_data(model_arch.normalize(model_arch.loadIN("all"), model_arch.loadOUT("all")))
    model_arch.model=model_arch.my_model_general(x_train)

    model_arch.compile_with(x_train)

    model_arch.fit_with(x_train,y_train,x_test,y_test,sc=model_type)
    model_arch.save_model() 

    # model_arch.print_result()
    return model_arch
def train_RESAC(name):

    
    model_type="RESAC"
    Nconv=5
    Nfilt=37
    epochs=150
    lr=-3
    batch_size=16
    
    test_num=0
    Incertitude=False
    end_layers=False
    pen=1e-3

    model_arch=modelStruct(name)
    model_arch.getpath()
    path_data=model_arch.params.path_data

    
    model_arch.addIN("SSH","R81","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R81.npy")
    model_arch.addIN("SST","R27","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R27.npy")
    model_arch.addIN("SST","R09","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R09.npy")
    model_arch.addIN("SST","R03","MinMaxScaler",path_data+"/1308_square_NATL60_SST_R03.npy")


    
    if Incertitude:
        loss="loss incertitude"
    else:
        loss="mse"
    
    model_arch.addOUT("SSH","R27","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R27.npy",loss)
    model_arch.addOUT("SSH","R09","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R09.npy",loss)
    model_arch.addOUT("SSH","R03","MinMaxScaler",path_data+"/1308_square_NATL60_SSH_R03.npy",loss)


    
    model_arch.printIN()
    model_arch.printOUT()
    
    
   

    model_arch.set_nn(Nconv=Nconv, Nfilt=Nfilt, function=0.1,model_type=model_type,Incertitude=Incertitude,end_layers=end_layers,pen=pen)
    model_arch.set_trainparam(lr=lr, epochs=epochs, batch_size=batch_size,test_num=test_num,use_scheduler=True)

    l=366+122
    model_arch.split_train_test(list(np.arange(l)),mode="1308")
    x_train,y_train,x_test,y_test=model_arch.prepare_data(model_arch.normalize(model_arch.loadIN("all"), model_arch.loadOUT("all")))
    model_arch.model=model_arch.my_model_general(x_train)

    model_arch.compile_with(x_train)

    model_arch.fit_with(x_train,y_train,x_test,y_test,sc=model_type)
    model_arch.save_model() 

    # model_arch.print_result()
    return model_arch



def train_denoising_model(name_model,batch_size, lr):



    
    model_arch=reload_model("EXP10pp/EPX10Subpix"+str(name_model))

    model_arch.convert_model_from_gpu()
    
    X=model_arch.loadIN("all")
    Y=model_arch.loadOUT("all")
    X,Y=model_arch.normalize(X,Y)
    
    
    
    [ind_train,ind_test]=[model_arch.params.train_param.ind_train,model_arch.params.train_param.ind_test]
    
    # Définition des jeus de test et train
    x_train=[]
    x_valid=[]
    y_train=[]
    y_valid=[]
    
    for tab in X:
        x_train.append(tab[ind_train])
        x_valid.append(tab[ind_test])
    
    for tab in Y:
        y_train.append((np.expand_dims(tab[ind_train], -1)))
        y_valid.append(np.expand_dims(tab[ind_test], -1))
    
    pred_train=model_arch.model.predict(x_train)[-1]
    pred_valid=model_arch.model.predict(x_valid)[-1]
    y_train=y_train[-1]
    y_valid=y_valid[-1]
    
    
    
    
    score=np.mean(realrmse2008(y_valid, pred_valid).numpy())
    
    
    model=denoising_model()
    model.summary()
    print("SCORE = ",score)
    
    
    optimizer = optimizers.Adam(learning_rate=lr)
    
    lr_metric = get_lr_metric(optimizer)
    
    
    reduce_lr = tensorflow.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=2, min_lr=0.000001)
    callback=[reduce_lr]
    
    model.compile(loss=["mse"],optimizer=optimizer,metrics=[lr_metric,realrmse2008])
    H=model.fit(pred_train,y_train,batch_size=batch_size,epochs=25,verbose=1,validation_data=(pred_valid,y_valid),callbacks=callback)
    Res=np.min(H.history["val_realrmse2008"])
    #Hist[i][j]=H.history["val_realrmse2008"]
    y_final=model.predict(pred_valid)
    plt.figure()
    plt.imshow(y_final[15]-y_valid[15])
    
    plt.figure()
    plt.plot(H.history["val_realrmse2008"])
    name=str(n_model)+"_Denoising_network"
    if not os.path.exists(PATH+name):
        os.makedirs(PATH+name)
    model_json = model.to_json()
    with open(PATH+name+"/model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(PATH+name+"/model.h5")
    np.save(PATH+name+"/history", H.history)
    return model
if __name__=="__main__":
       pass
    # train_RESACsub_supBNsub("EXP14subpix_withoutsubBNsub",15)
    # train_RESACsub_BN("EXP14subpix_withoutsubBNsub",15)
    # train_RESAC("EXP14subpix_withoutsubBNsub",15)
    # train_denoising_model(7,1, 1e-5)
