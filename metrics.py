#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 11:01:32 2021

@author: archambault
"""


import math
from   matplotlib  import cm
import sys
from   time  import time
import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import random 
from sklearn.preprocessing import StandardScaler
import pickle
from scipy.signal import convolve2d
from tensorflow.keras.layers import Input, Dense, Flatten, Reshape, AveragePooling2D, Dropout, add 
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D #, Deconvolution2D
from tensorflow.keras.layers import Concatenate, concatenate, BatchNormalization,Lambda ,Activation
from tensorflow.keras.models    import Model
from tensorflow.keras.models    import load_model, model_from_json
import tensorflow.keras.callbacks
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras import regularizers
from tensorflow.keras import optimizers
from functools import partial
from tensorflow.nn import swish
from tensorflow.nn import depth_to_space
from tensorflow.nn import space_to_depth
from tensorflow.keras.callbacks import LearningRateScheduler


from tensorflow.keras import backend as K


def get_lr_metric(optimizer):
    def lr(y_true, y_pred):
        return optimizer._decayed_lr(tensorflow.float32) # I use ._decayed_lr method instead of .lr
    return lr


def realrmse2008(y_true, y_pred):
    mean_true = y_true[:,:,:,0]
    mean_pred = y_pred[:,:,:,0]
    loss = tensorflow.math.sqrt(tensorflow.reduce_sum(tensorflow.math.squared_difference(mean_true, mean_pred ),[1,2])/(y_pred.shape[1]*y_pred.shape[2]))
    
    s=y_true.shape[1]
    if s==48:
        return loss*(1.3279966115951538--0.7570118308067322)
    if s==144:
        return loss*(1.3510133028030396--0.856661319732666)

    if s==432:
        return loss*(1.3550180196762085--0.8636236190795898)
    
    if s==1296:
        return loss*(1.3550180196762085--0.8636236190795898)
    
    
def psnr(y_true, y_pred):
    mean_true = y_true[:,:,:,0]
    mean_pred = y_pred[:,:,:,0]
    return tensorflow.image.psnr(mean_true, mean_pred, 1, name=None)


def R2(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )


def realrmse2008_decile(y_true, y_pred):
    
    RMSE=[[],[]]
    for k in range(y_true.shape[0]):
        yt=np.ndarray.flatten(y_true[k,:,:,0])
        yp=np.ndarray.flatten(y_pred[k,:,:,0])
        seuil=np.percentile(y_true,[10,90])
        seuil10,seuil90=seuil[0],seuil[1]
        
        
        yt90=[]
        yp90=[]
        yt10=[]
        yp10=[]
        
        for i in range(yt.shape[0]):
            if yt[i]>=seuil90:
                yt90.append(yt[i])
                yp90.append(yp[i])
            elif yt[i]<=seuil10:
                yt10.append(yt[i])
                yp10.append(yp[i])
                
        mse90=np.square(np.array(yt90)-np.array(yp90)).mean()
        rmse90=(mse90**(1/2))*(1.3550180196762085--0.8636236190795898)
        mse10=np.square(np.array(yt10)-np.array(yp10)).mean()
        rmse10=(mse10**(1/2))*(1.3550180196762085--0.8636236190795898)
        RMSE[0].append(rmse10)
        RMSE[1].append(rmse90)

    return RMSE
       


def realrmse2008_crop(y_true, y_pred):
    
    RMSE=[]
    for k in range(y_true.shape[0]):
        
        
        yt=y_true[k,6:y_pred.shape[1]-6,6:y_pred.shape[2]-6,0]
        yp=y_pred[k,6:y_pred.shape[1]-6,6:y_pred.shape[2]-6,0]
                
        mse=np.square(np.array(yt)-np.array(yp)).mean()
        rmse=(mse**(1/2))*(1.3550180196762085--0.8636236190795898)
        RMSE.append(rmse)

    return RMSE
       

def loss_mle(y_true, y_pred):
        mean_true = y_true[:,:,:,0]
        mean_pred, std_pred = y_pred[:,:,:,0], y_pred[:,:,:,1]
        # Max to prevent NaN's and Inf's
        log_std = tensorflow.math.log(tensorflow.maximum(std_pred,tensorflow.math.sqrt(tensorflow.keras.backend.epsilon()*1000)))
        sq_std = tensorflow.maximum(tensorflow.square(std_pred),tensorflow.keras.backend.epsilon()*1000 )
        mse = tensorflow.math.squared_difference(mean_pred, mean_true) /tensorflow.maximum(2*sq_std,tensorflow.keras.backend.epsilon()*1000)
        loss = tensorflow.math.add(mse, log_std )
        loss = tensorflow.reduce_sum(loss, -1)
        return loss

#Need a special loss

# def checkerboard_artifacts(y_true, y_pred):
#     diff=y_true-y_pred
    
#     I=np.zeros((diff.shape[0]-6,diff.shape[1]-6))
#     for i in range (I.shape[0]):
#         for j in range (I.shape[1]):
            
#             c1=diff[i:i+3,j:j+3]
#             c1=(c1-np.mean(c1))/max(np.std(c1),0.0000000001)
#             c2=diff[i:i+3,j+3:j+6]
#             c2=(c2-np.mean(c2))/max(np.std(c2),0.0000000001)
#             c3=diff[i:i+3,j+6:j+9]
#             c3=(c3-np.mean(c3))/max(np.std(c3),0.0000000001)

            
            
#             c4=diff[i+3:i+6,j:j+3]
#             c4=(c4-np.mean(c4))/max(np.std(c4),0.0000000001)
#             c5=diff[i+3:i+6,j+3:j+6]
#             c5=(c5-np.mean(c5))/max(np.std(c5),0.0000000001)
#             c6=diff[i+3:i+6,j+6:j+9]
#             c6=(c6-np.mean(c6))/max(np.std(c6),0.0000000001)

            
#             c7=diff[i+6:i+9,j:j+3]
#             c7=(c7-np.mean(c7))/max(np.std(c7),0.0000000001)
#             c8=diff[i+6:i+9,j+3:j+6]
#             c8=(c8-np.mean(c8))/max(np.std(c8),0.0000000001)
#             c9=diff[i+6:i+9,j+6:j+9]
#             c9=(c9-np.mean(c9))/max(np.std(c9),0.0000000001)
            
#             print(c1)
#             print(c1.shape)
            
#             print(c2.shape)
#             print(c1)

#             print(c3.shape)
#             print(c4.shape)
#             print(c5.shape)
#             print(c6.shape)
#             print(c7.shape)
#             print(c8.shape)
#             print(c9.shape)

#             Zone=np.concatenate((np.concatenate((c1,c2,c3),axis=1),np.concatenate((c4,c5,c6),axis=1),np.concatenate((c7,c8,c9),axis=1)),axis=0)
            
#             print(c1)
#             print(c2)
#             print(c3)

#             print(Zone)
#             print(Zone.shape)
#             plt.Figure()
#             plt.imshow(Zone)
#             conv=convolve2d(Zone,c5)
#             plt.Figure()
#             plt.imshow(conv)
            
#             return
            
#             # Exemple:
#             #            c1   c2   c3
#             #            c4   c5   c6
#             #            c7   c8   c9   
    
    


# checkerboard_artifacts(y_true, y_pred)

    
def loss_mle_new(y_true, y_pred):
    EPSI = 1e-3 #Minimum value for the standard deviation (avoid NaN's and Inf's)
    
    mean_true = y_true[:,:,:,0]
    mean_pred, std_pred = y_pred[:,:,:,0], tensorflow.maximum((y_pred[:,:,:,1]), EPSI*tensorflow.ones(tensorflow.shape(y_true[:,:,:,0])))
    # Max to prevent NaN's and Inf's
    log_std = tensorflow.math.log(std_pred)
    sq_std = tensorflow.square(std_pred)
    mse = tensorflow.math.squared_difference(mean_pred, mean_true) / (2*sq_std)
    loss = log_std + mse
    loss = tensorflow.reduce_sum(loss, -1)
    return loss

def loss_mle_theo(y_true, y_pred):
    EPSI = 1e-6 #Minimum value for the standard deviation (avoid NaN's and Inf's)
    
    mean_true = y_true[:,:,:,0]
    mean_pred = y_pred[:,:,:,0]
    std_pred_squared = tensorflow.maximum(tensorflow.square(y_pred[:,:,:,1]), EPSI*tensorflow.ones(tensorflow.shape(y_true[:,:,:,0])))
    # Max to prevent NaN's and Inf's
    log_std = 1/2*tensorflow.math.log(std_pred_squared)
    mse = 1/2*tensorflow.math.squared_difference(mean_pred, mean_true) / (2*std_pred_squared)
    loss = log_std + mse
    loss = tensorflow.reduce_sum(loss, [1,2])
    return loss